//
//  Encodable+Extensions.swift
//  RKFoundation
//
//  Created by Reha Kilic on 18.01.2019.
//  Copyright © 2020 RK. All rights reserved.
//

import Foundation

public extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }

    var data: Data? {
        return try? RKEncoder().encode(self)
    }

    var dictionary: [String: Any] {
        guard let data = data,
              let jsonObject = try? JSONSerialization.jsonObject(with: data),
              let dictionary = jsonObject as? [String: Any]
        else { return [:] }
        return dictionary
    }

}
