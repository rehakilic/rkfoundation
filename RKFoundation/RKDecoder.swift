//
//  RKDecoder.swift
//  RK
//
//  Created by Reha Kilic on 21.01.2019.
//  Copyright © 2020 RK. All rights reserved.
//

import Foundation

public class RKDecoder: JSONDecoder {
    private enum DateError: String, Error {
        case invalidDate
    }

    public override init() {
        super.init()
        self.dateDecodingStrategy = .custom({ (decoder) -> Date in
            let formatter = DateFormatter()
            formatter.calendar = Calendar(identifier: .iso8601)
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.timeZone = TimeZone(secondsFromGMT: 0)

            let container = try decoder.singleValueContainer()
            let dateStr = try container.decode(String.self)

            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            if let date = formatter.date(from: dateStr) {
                return date
            }
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssXXXXX"
            if let date = formatter.date(from: dateStr) {
                return date
            }
            throw DateError.invalidDate
        })
    }
}
