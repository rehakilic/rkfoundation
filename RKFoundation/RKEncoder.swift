//
//  RKEncoder.swift
//  RK
//
//  Created by Reha Kilic on 21.01.2019.
//  Copyright © 2020 RK. All rights reserved.
//

import Foundation

public class RKEncoder: JSONEncoder {
    public override init() {
        super.init()
        self.dateEncodingStrategy = .iso8601
    }
}
