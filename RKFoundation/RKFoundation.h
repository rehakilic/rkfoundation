//
//  RKFoundation.h
//  RKFoundation
//
//  Created by Reha Kilic on 23.04.2020.
//  Copyright © 2020 RK. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for RKFoundation.
FOUNDATION_EXPORT double RKFoundationVersionNumber;

//! Project version string for RKFoundation.
FOUNDATION_EXPORT const unsigned char RKFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RKFoundation/PublicHeader.h>


